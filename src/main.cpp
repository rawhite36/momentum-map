

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <TFile.h>
#include <TTree.h>

Char_t ringSelect(Double_t, Double_t);
UChar_t pixSelect(Double_t, Double_t);

int main(int argc, char** argv)
{
	const Double_t spdLight{2.99792e8}; //m/s
	const Double_t massProt{9.38272e5}; //keV
  Double_t**** momMap;
	std::string inFile{};
	std::string outFile{};
	Long64_t numEntTemp{};
	Long64_t numEnt;
	TFile* fIn;
	std::ofstream* fOut;
  TTree* treeTemp[2];
  std::vector<Double_t>* Hit_timeB{}; //ns
  std::vector<Double_t>* Hit_energyB{}; //keV
  std::vector<Double_t>* Hit_xB{}; //mm
  std::vector<Double_t>* Hit_yB{}; //mm
  std::vector<Int_t>* Hit_detectorNbB{};
  std::vector<Int_t>* Hit_particleTypeB{};
  std::vector<Int_t>* Hit_parentIDB{};
  Double_t eE0B; //keV
  Double_t pE0B; //eV
	Double_t pTimeB; //us
	Double_t pMomB; //keV
	Double_t eEnergyB; //keV
  Double_t sortD;
  Int_t sortI;
  Double_t timeTemp;
  Char_t flagTemp[2];
	Double_t pPosTemp[2];
	Char_t ring;

  if(argc == 3 || argc == 4)
  {
    for(Short_t i{}; i < strlen(argv[1]); ++i)
      inFile += {argv[1][i]};
    for(Short_t i{}; i < strlen(argv[2]); ++i)
      outFile += {argv[2][i]};

		if(argc == 4)
			numEntTemp = {atoi(argv[3])};

		fIn = {new TFile{inFile.c_str()}};

	  if(fIn->IsOpen() && fIn->GetListOfKeys()->Contains("dynamicTree"))
	  {
			momMap = {new Double_t***[150]};

	  	for(UChar_t i{}; i < 150; ++i)
			{
	    	momMap[i] = {new Double_t**[40]};

	    	for(Char_t j{}; j < 40; ++j)
	    	{
	      	momMap[i][j] = {new Double_t*[8]};

	      	for(UChar_t k{}; k < 8; ++k)
	      	{
	        	momMap[i][j][k] = {new Double_t[3]};

	        	momMap[i][j][k][0] = {};
	        	momMap[i][j][k][1] = {};
	        	momMap[i][j][k][2] = {};
					}
				}
			}

			treeTemp[0] = {dynamic_cast<TTree*>(fIn->Get("dynamicTree"))};

		  treeTemp[0]->SetBranchAddress("Hit_time", &Hit_timeB);
		  treeTemp[0]->SetBranchAddress("Hit_energy", &Hit_energyB);
		  treeTemp[0]->SetBranchAddress("Hit_x", &Hit_xB);
		  treeTemp[0]->SetBranchAddress("Hit_y", &Hit_yB);
		  treeTemp[0]->SetBranchAddress("Hit_detectorNb", &Hit_detectorNbB);
		  treeTemp[0]->SetBranchAddress("Hit_particleType", &Hit_particleTypeB);
		  treeTemp[0]->SetBranchAddress("Hit_parentID", &Hit_parentIDB);
		  treeTemp[0]->SetBranchAddress("eE0", &eE0B);
		  treeTemp[0]->SetBranchAddress("pE0", &pE0B);

			treeTemp[1] = {new TTree("cData", "Coincidence Data")};
			treeTemp[1]->SetDirectory(0);

			treeTemp[1]->Branch("pTime", &pTimeB, "pTimeL/D"); //us
			treeTemp[1]->Branch("pMom", &pMomB, "pMomL/D"); //MeV^2
			treeTemp[1]->Branch("eEnergy", &eEnergyB, "eEnergyL/D"); //keV

			if(numEntTemp < 1 || numEntTemp > treeTemp[0]->GetEntries())
				numEnt = {treeTemp[0]->GetEntries()};
			else
				numEnt = {numEntTemp};

  	  for(Long64_t i{}; i < numEnt; ++i)
	    {
	      treeTemp[0]->GetEntry(i);

				for(Int_t j{1}; j > 0;)
				{
					j = {};

					for(Int_t k{1}; k < Hit_timeB->size(); ++k)
						if((*Hit_timeB)[k] < (*Hit_timeB)[k - 1])
						{
							sortD = {(*Hit_timeB)[k]};
							(*Hit_timeB)[k] = {(*Hit_timeB)[k - 1]};
							(*Hit_timeB)[k - 1] = {sortD};

							sortD = {(*Hit_energyB)[k]};
							(*Hit_energyB)[k] = {(*Hit_energyB)[k - 1]};
							(*Hit_energyB)[k - 1] = {sortD};

							sortD = {(*Hit_xB)[k]};
							(*Hit_xB)[k] = {(*Hit_xB)[k - 1]};
							(*Hit_xB)[k - 1] = {sortD};

							sortD = {(*Hit_yB)[k]};
							(*Hit_yB)[k] = {(*Hit_yB)[k - 1]};
							(*Hit_yB)[k - 1] = {sortD};

							sortI = {(*Hit_detectorNbB)[k]};
							(*Hit_detectorNbB)[k] = {(*Hit_detectorNbB)[k - 1]};
							(*Hit_detectorNbB)[k - 1] = {sortI};

							sortI = {(*Hit_particleTypeB)[k]};
							(*Hit_particleTypeB)[k] = {(*Hit_particleTypeB)[k - 1]};
							(*Hit_particleTypeB)[k - 1] = {sortI};

							sortI = {(*Hit_parentIDB)[k]};
							(*Hit_parentIDB)[k] = {(*Hit_parentIDB)[k - 1]};
							(*Hit_parentIDB)[k - 1] = {sortI};

							++j;
						}
				}

	      timeTemp = {};
	      flagTemp[0] = {};
	      flagTemp[1] = {};

	      for(Int_t j{}; j < Hit_timeB->size(); ++j)
	        if((*Hit_particleTypeB)[j] == 1 && !(*Hit_parentIDB)[j]
	        && (*Hit_detectorNbB)[j] == 1)
	        {
	          timeTemp = {(*Hit_timeB)[j]};
	          pPosTemp[0] = {(*Hit_xB)[j]};
	          pPosTemp[1] = {(*Hit_yB)[j]};
	          flagTemp[0] = {1};
	          j = {static_cast<Int_t>(Hit_timeB->size())};
	        }

	      for(Int_t j{}; j < Hit_timeB->size(); ++j)
	        if((*Hit_particleTypeB)[j] == 2 || (*Hit_particleTypeB)[j] == 3)
	          flagTemp[1] = {1};

	      if(flagTemp[0] && flagTemp[1])
	      {
	        pTimeB = {1e-3*timeTemp};
	        eEnergyB = {eE0B};
	        pMomB = {sqrt(1e-6*pE0B*pE0B + 2e-3*massProt*pE0B)};
					ring = {ringSelect(pPosTemp[0], pPosTemp[1])};

					for(UChar_t i{}; i < 150; ++i)
						if(1/pTimeB/pTimeB >= 4.2e-5*i && 1/pTimeB/pTimeB < 4.2e-5*(i + 1))
							for(UChar_t j{}; j < 40; ++j)
								if(eEnergyB >= 20.*j && eEnergyB < 20.*(j + 1))
								{
									momMap[i][j][ring][0] += 1e-6*spdLight*pTimeB*pMomB/sqrt(pMomB*pMomB
									+ massProt*massProt);
									momMap[i][j][ring][1] += 1e-12*spdLight*spdLight*pTimeB*pTimeB*pMomB
									*pMomB/(pMomB*pMomB + massProt*massProt);
									++momMap[i][j][ring][2];
								}
				}
			}

			delete fIn;

			for(UChar_t i{}; i < 150; ++i)
				for(Char_t j{}; j < 40; ++j)
					for(Char_t k{}; k < 8; ++k)
						if(momMap[i][j][k][2] > .5)
						{
							momMap[i][j][k][0] /= momMap[i][j][k][2];

							if(momMap[i][j][k][2] > 1.5)
								momMap[i][j][k][1] = {sqrt((momMap[i][j][k][1] - momMap[i][j][k][2]
								*momMap[i][j][k][0]*momMap[i][j][k][0])/(momMap[i][j][k][2] - 1))};
							else
								momMap[i][j][k][1] = {};
						}

			fOut = {new std::ofstream{outFile, std::ofstream::trunc | std::ofstream::binary}};

			if(fOut->is_open())
				for(UChar_t i{}; i < 150; ++i)
					for(Char_t j{}; j < 40; ++j)
						for(Char_t k{}; k < 8; ++k)
						{
							fOut->write(reinterpret_cast<Char_t*>(&momMap[i][j][k][0]), 8);
							fOut->write(reinterpret_cast<Char_t*>(&momMap[i][j][k][1]), 8);
							fOut->write(reinterpret_cast<Char_t*>(&momMap[i][j][k][2]), 8);
						}
			else
				std::cout << "Error: Cannot Open Output File: " << outFile << '\n';

			delete fOut;

			for(UChar_t i{}; i < 150; ++i)
			{
				for(UChar_t j{}; j < 40; ++j)
				{
					for(UChar_t k{}; k < 8; ++k)
						delete[] momMap[i][j][k];

					delete[] momMap[i][j];
				}

				delete[] momMap[i];
			}

			delete[] momMap;
			delete treeTemp[1];
		}
		else
		{
			std::cout << "Error: Cannot Open Input File: " << inFile
			<< "       Or 'dynamicTree' Does Not Exist Within File\n";
			delete fIn;
		}
	}
}

//--------------------------------------------------------------------

Char_t ringSelect(Double_t x, Double_t y)
{
  UChar_t pixel{pixSelect(x, y)};

  if(pixel == 64)
    return 0;
  else if(pixel == 51 || pixel == 52 || pixel == 63 || pixel == 65 || pixel == 76 || pixel == 77)
    return 1;
  else if((pixel > 38 && pixel < 42) || pixel == 50 || pixel == 53 || pixel == 62 || pixel == 66
          || pixel == 75 || pixel == 78 || (pixel > 86 && pixel < 90))
    return 2;
  else if((pixel > 27 && pixel < 32) || pixel == 38 || pixel == 42 || pixel == 49 || pixel == 54
          || pixel == 61 || pixel == 67 || pixel == 74 || pixel == 79 || pixel == 86 || pixel == 90
          || (pixel > 96 && pixel < 101))
    return 3;
  else if((pixel > 17 && pixel < 23) || pixel == 27 || pixel == 32 || pixel == 37 || pixel == 43
          || pixel == 48 || pixel == 55 || pixel == 60 || pixel == 68 || pixel == 73 || pixel == 80
          || pixel == 85 || pixel == 91 || pixel == 96 || pixel == 101 || (pixel > 105
          && pixel < 111))
    return 4;
  else if((pixel > 8 && pixel < 15) || pixel == 17 || pixel == 23 || pixel == 26 || pixel == 33
          || pixel == 36 || pixel == 44 || pixel == 47 || pixel == 56 || pixel == 59 || pixel == 69
          || pixel == 72 || pixel == 81 || pixel == 84 || pixel == 92 || pixel == 95 || pixel == 102
          || pixel == 105 || pixel == 111 || (pixel > 113 && pixel < 120))
    return 5;
  else if((pixel > 0 && pixel <= 8) || pixel == 15 || pixel == 16 || pixel == 24 || pixel == 25
          || pixel == 34 || pixel == 35 || pixel == 45 || pixel == 46 || pixel == 57 || pixel == 58
          || pixel == 70 || pixel == 71 || pixel == 82 || pixel == 83 || pixel == 93 || pixel == 94
          || pixel == 103 || pixel == 104 || pixel == 112 || pixel == 113 || (pixel >= 120
          && pixel < 128))
    return 6;
  else
    return 7;
}

//--------------------------------------------------------------------

UChar_t pixSelect(Double_t x, Double_t y)
{
  Double_t px[6] = {22.998,68.993,114.987,160.982,206.977,252.972};
  Double_t py[7] = {23.334,70.001,116.668,163.334,210.002,256.668};
  UChar_t p[2];

  if(x<-px[5])
    p[0]=0;
  else if(x<-px[4])
    p[0]=1;
  else if(x<-px[3])
    p[0]=2;
  else if(x<-px[2])
    p[0]=3;
  else if(x<-px[1])
    p[0]=4;
  else if(x<-px[0])
    p[0]=5;
  else if(x<px[0])
    p[0]=6;
  else if(x<px[1])
    p[0]=7;
  else if(x<px[2])
    p[0]=8;
  else if(x<px[3])
    p[0]=9;
  else if(x<px[4])
    p[0]=10;
  else if(x<px[5])
    p[0]=11;
  else
    p[0]=12;

  switch(p[0])
  {
    case 0:
      if(y>py[2])
        p[1]=1;
      else if(y>py[1])
        p[1]=2;
      else if(y>py[0])
        p[1]=3;
      else if(y>-py[0])
        p[1]=4;
      else if(y>-py[1])
        p[1]=5;
      else if(y>-py[2])
        p[1]=6;
      else
        p[1]=7;
      break;
    case 1:
      if(y>py[2]+23.3335)
        p[1]=8;
      else if(y>py[1]+23.3335)
        p[1]=9;
      else if(y>py[0]+23.3335)
        p[1]=10;
      else if(y>0)
        p[1]=11;
      else if(y>-py[1]+23.3335)
        p[1]=12;
      else if(y>-py[2]+23.3335)
        p[1]=13;
      else if(y>-py[3]+23.3335)
        p[1]=14;
      else
        p[1]=15;
      break;
    case 2:
      if(y>py[3])
        p[1]=16;
      else if(y>py[2])
        p[1]=17;
      else if(y>py[1])
        p[1]=18;
      else if(y>py[0])
        p[1]=19;
      else if(y>-py[0])
        p[1]=20;
      else if(y>-py[1])
        p[1]=21;
      else if(y>-py[2])
        p[1]=22;
      else if(y>-py[3])
        p[1]=23;
      else
        p[1]=24;
      break;
    case 3:
      if(y>py[3]+23.3335)
        p[1]=25;
      else if(y>py[2]+23.3335)
        p[1]=26;
      else if(y>py[1]+23.3335)
        p[1]=27;
      else if(y>py[0]+23.3335)
        p[1]=28;
      else if(y>0)
        p[1]=29;
      else if(y>-py[1]+23.3335)
        p[1]=30;
      else if(y>-py[2]+23.3335)
        p[1]=31;
      else if(y>-py[3]+23.3335)
        p[1]=32;
      else if(y>-py[4]+23.3335)
        p[1]=33;
      else
        p[1]=34;
      break;
    case 4:
      if(y>py[4])
        p[1]=35;
      else if(y>py[3])
        p[1]=36;
      else if(y>py[2])
        p[1]=37;
      else if(y>py[1])
        p[1]=38;
      else if(y>py[0])
        p[1]=39;
      else if(y>-py[0])
        p[1]=40;
      else if(y>-py[1])
        p[1]=41;
      else if(y>-py[2])
        p[1]=42;
      else if(y>-py[3])
        p[1]=43;
      else if(y>-py[4])
        p[1]=44;
      else
        p[1]=45;
      break;
    case 5:
      if(y>py[4]+23.3335)
        p[1]=46;
      else if(y>py[3]+23.3335)
        p[1]=47;
      else if(y>py[2]+23.3335)
        p[1]=48;
      else if(y>py[1]+23.3335)
        p[1]=49;
      else if(y>py[0]+23.3335)
        p[1]=50;
      else if(y>0)
        p[1]=51;
      else if(y>-py[1]+23.3335)
        p[1]=52;
      else if(y>-py[2]+23.3335)
        p[1]=53;
      else if(y>-py[3]+23.3335)
        p[1]=54;
      else if(y>-py[4]+23.3335)
        p[1]=55;
      else if(y>-py[5]+23.3335)
        p[1]=56;
      else
        p[1]=57;
      break;
    case 6:
      if(y>py[5])
        p[1]=58;
      else if(y>py[4])
        p[1]=59;
      else if(y>py[3])
        p[1]=60;
      else if(y>py[2])
        p[1]=61;
      else if(y>py[1])
        p[1]=62;
      else if(y>py[0])
        p[1]=63;
      else if(y>-py[0])
        p[1]=64;
      else if(y>-py[1])
        p[1]=65;
      else if(y>-py[2])
        p[1]=66;
      else if(y>-py[3])
        p[1]=67;
      else if(y>-py[4])
        p[1]=68;
      else if(y>-py[5])
        p[1]=69;
      else
        p[1]=70;
      break;
    case 7:
      if(y>py[4]+23.3335)
        p[1]=71;
      else if(y>py[3]+23.3335)
        p[1]=72;
      else if(y>py[2]+23.3335)
        p[1]=73;
      else if(y>py[1]+23.3335)
        p[1]=74;
      else if(y>py[0]+23.3335)
        p[1]=75;
      else if(y>0)
        p[1]=76;
      else if(y>-py[1]+23.3335)
        p[1]=77;
      else if(y>-py[2]+23.3335)
        p[1]=78;
      else if(y>-py[3]+23.3335)
        p[1]=79;
      else if(y>-py[4]+23.3335)
        p[1]=80;
      else if(y>-py[5]+23.3335)
        p[1]=81;
      else
        p[1]=82;
      break;
    case 8:
      if(y>py[4])
        p[1]=83;
      else if(y>py[3])
        p[1]=84;
      else if(y>py[2])
        p[1]=85;
      else if(y>py[1])
        p[1]=86;
      else if(y>py[0])
        p[1]=87;
      else if(y>-py[0])
        p[1]=88;
      else if(y>-py[1])
        p[1]=89;
      else if(y>-py[2])
        p[1]=90;
      else if(y>-py[3])
        p[1]=91;
      else if(y>-py[4])
        p[1]=92;
      else
        p[1]=93;
      break;
    case 9:
      if(y>py[3]+23.3335)
        p[1]=94;
      else if(y>py[2]+23.3335)
        p[1]=95;
      else if(y>py[1]+23.3335)
        p[1]=96;
      else if(y>py[0]+23.3335)
        p[1]=97;
      else if(y>0)
        p[1]=98;
      else if(y>-py[1]+23.3335)
        p[1]=99;
      else if(y>-py[2]+23.3335)
        p[1]=100;
      else if(y>-py[3]+23.3335)
        p[1]=101;
      else if(y>-py[4]+23.3335)
        p[1]=102;
      else
        p[1]=103;
      break;
    case 10:
      if(y>py[3])
        p[1]=104;
      else if(y>py[2])
        p[1]=105;
      else if(y>py[1])
        p[1]=106;
      else if(y>py[0])
        p[1]=107;
      else if(y>-py[0])
        p[1]=108;
      else if(y>-py[1])
        p[1]=109;
      else if(y>-py[2])
        p[1]=110;
      else if(y>-py[3])
        p[1]=111;
      else
        p[1]=112;
      break;
    case 11:
      if(y>py[2]+23.3335)
        p[1]=113;
      else if(y>py[1]+23.3335)
        p[1]=114;
      else if(y>py[0]+23.3335)
        p[1]=115;
      else if(y>0)
        p[1]=116;
      else if(y>-py[1]+23.3335)
        p[1]=117;
      else if(y>-py[2]+23.3335)
        p[1]=118;
      else if(y>-py[3]+23.3335)
        p[1]=119;
      else
        p[1]=120;
      break;
    case 12:
      if(y>py[2])
        p[1]=121;
      else if(y>py[1])
        p[1]=122;
      else if(y>py[0])
        p[1]=123;
      else if(y>-py[0])
        p[1]=124;
      else if(y>-py[1])
        p[1]=125;
      else if(y>-py[2])
        p[1]=126;
      else
        p[1]=127;
      break;
  }

  return p[1];
}
