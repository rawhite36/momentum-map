

bin/createMomMap: build/main.o
	g++ -std=c++11 -o bin/createMomMap build/main.o `root-config --libs`

build/main.o: src/main.cpp
	g++ -c -std=c++11 -o build/main.o src/main.cpp `root-config --cflags`

clean:
	rm build/* bin/*
